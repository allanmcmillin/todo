<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Todo Site</title>
    </head>
    <body>
        <h1>Todo's</h1>
        <ul>
                <?php foreach ($todos as $todo): ?>
                    <li>
                        <?php echo $todo->content; ?>
                    </li>
                <?php endforeach ?>
        </ul>

    </body>
</html>
