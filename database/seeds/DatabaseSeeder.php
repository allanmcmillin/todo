<?php

use Illuminate\Database\Seeder;
use App\Todo;
use Faker\Factory;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $todos = new Todo();
        $todos->content = $faker->bs;
        $todos->save();
        // $this->call(UsersTableSeeder::class);
    }
}
